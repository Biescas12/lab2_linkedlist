package linkedLists;
/**
 * Singly linked list with references to its first and its
 * last node. 
 * 
 * @author pirvos
 *
 */

import java.lang.reflect.Array;

import linkedLists.LinkedList;

public class SLFLList<E> 
implements LinkedList<E>
{

	private SNode<E> first, last; 
	int length = 0; 
	
	public SLFLList() { 
		first = last = null; 
	}
	
	
	public void addFirstNode(Node<E> nuevo) {
		((SNode<E>) nuevo).setNext(first);
		first = (SNode<E>) nuevo;
		
		if(length == 0) {
			last = (SNode<E>) nuevo;
		}
		
		length++;
		
	}

	public void addNodeAfter(Node<E> target, Node<E> nuevo) {
		if(length == 0) {
			addFirstNode(nuevo);
		}
		if(target == last) {
			((SNode<E>) nuevo).setNext(null);
			((SNode<E>) target).setNext((SNode<E>) nuevo);
			last = (SNode<E>) nuevo;
		}
		((SNode<E>) nuevo).setNext(((SNode<E>) target).getNext()); 
		((SNode<E>) target).setNext((SNode<E>) nuevo); 
		
		length++;
	}

	public void addNodeBefore(Node<E> target, Node<E> nuevo) {
//		if(length == 0) {
//			addFirstNode(nuevo);
//		}
		if(target == first) {
			this.addFirstNode(nuevo);
		}
		else {
			Node<E> prev = findNodePrevTo(target);
			this.addNodeAfter(prev, nuevo);
		}
		
		
	}

	public Node<E> getFirstNode() throws NodeOutOfBoundsException {
		if(first == null) {
			throw new NodeOutOfBoundsException("List is empty...");
		}
		return first;
	}

	public Node<E> getLastNode() throws NodeOutOfBoundsException {
		if(last == null) {
			throw new NodeOutOfBoundsException("List is empty...");
		}
		return last;
	}

	public Node<E> getNodeAfter(Node<E> target) throws NodeOutOfBoundsException {
		if(target == last) {
			throw new NodeOutOfBoundsException("target is last...");
		}
		SNode<E> aNode = ((SNode<E>) target).getNext();
		return aNode;
	}

	public Node<E> getNodeBefore(Node<E> target)
			throws NodeOutOfBoundsException {
		if(target == first) {
			throw new NodeOutOfBoundsException("target is first...");

		}
		return findNodePrevTo(target);
	}

	public int length() {
		return this.length;
	}

	public void removeNode(Node<E> target) {
		
		if(this.length == 0) {
			first = null;
			last = null;
		}
		
		if(target == first) {
			SNode<E> ntr = first;
			first = first.getNext();
			ntr.setNext(null);
		}
		
		else if(target == last) {
			SNode<E> prev = (SNode<E>) this.getNodeBefore(target);
			prev.setNext(null);
			last = prev;
		}
		else {
			SNode<E> prev = (SNode<E>) this.getNodeBefore(target);
			prev.setNext(((SNode<E>) target).getNext());
		}
		length--;
	}
	
	public Node<E> createNewNode() {
		return new SNode<E>();
	}
	
	private Node<E> findNodePrevTo(Node<E> target) {
		// Pre: target is a node in the list
		if (target == first) 
			return null; 
		else { 
			SNode<E> prev = first; 
			while (prev != null && prev.getNext() != target) 
				prev = prev.getNext();  
			return prev; 
		}
	}
	
	Object[] toArray() {
		Object[] array = new Object[this.length];
		SNode<E> curr = first;
		int i = 0;
		while(curr != null) {
			array[i++] = curr.getElement();
			curr = curr.getNext();
		}

		return array;
	}
	
	@SuppressWarnings("unchecked")
	public <T1> T1[] toArray(T1[] array) {
		if(this.length > array.length) {
			array = (T1[]) Array.newInstance(array.getClass().getComponentType(), this.length);
		}
		else {
			for (int i = this.length(); i <array.length; i++) {
				array[i] = null;
			}
		}

		int i = 0;
		SNode<E> curr = first;
		while(curr != null) {
			array[i++] = (T1) curr.getElement();
			curr = curr.getNext();
		}
		return array;
	}
	
	///////////////////////////////////////////////////
	// private and static inner class that defines the 
	// type of node that this list implementation uses
	private static class SNode<T> implements Node<T> {
		private T element; 
		private SNode<T> next; 
		public SNode() { 
			element = null; 
			next = null; 
		}
		public SNode(T data, SNode<T> next) { 
			this.element = data; 
			this.next = next; 
		}
		public SNode(T data)  { 
			this.element = data; 
			next = null; 
		}
		public T getElement() {
			return element;
		}
		public void setElement(T data) {
			this.element = data;
		}
		public SNode<T> getNext() {
			return next;
		}
		public void setNext(SNode<T> next) {
			this.next = next;
		}
	}

}
